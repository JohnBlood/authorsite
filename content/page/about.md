---
title: "About"
description: ""
images: []
draft: false
menu: main
weight: 0
---

![](/../img/headshot.png "John Paul Wohlscheid")

John Paul was born and raised in West Michigan. He discovered detective stories at an early age through the magic of Old Time Radio. Since then he has devoured hundreds of hours of radio shows (such as Sam Spade, Philip Marlowe, Boston Blackie, Richard Diamond and Yours Truly, Johnny Dollar) and mystery stories. With all this knowledge, he decided to take a crack at recreating those hard-boiled stories of yesteryear. Someday he plans to expand into scifi and westerns and pick your own adventure games.

Websites I created:
-------------------

- [GitPi.us](https://gitpi.us/) -- my thoughts on tech

Websites I contribute to:
-------------------------

- [It's FOSS](https://itsfoss.com/author/john/) -- News about Linux and Open Source Software
- [FOSS Mint](https://www.fossmint.com/author/johnblood/) -- More Linux and Open Source
- [Klara System](https://klarasystems.com/articles/) -- Articles on the history of Unix and BSD
- [Maccabee Society](http://maccabeesociety.com/author/john-paul-wohlscheid/) -- A Journal and Community for Men
