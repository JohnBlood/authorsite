---
title: "Books Written by John Paul"
description: "Books written by John Paul Wohlscheid"
images: []
draft: false
# menu: main
weight: 0
---

# Fiction

## Don't Look a Gift Horse in the Mouth

[![](/../img/dont-look-a-gift-horse-in-the-mouth.png "Don't Look a Gift Horse in the Mouth ebook")](https://books2read.com/u/mvgA64)

Being a private eye means helping the little guy and that is what Benny Cahill does for a living. The problem is: this kind of work can be bad for your health. After helping a jockey put a bet on a fixed race, Benny loses the money and finds his client dead. With the cops eyeing him for the hot seat, Benny Cahill must bring the real killers to justice. All because a jockey had never heard of the phrase: "Don't Look a Gift Horse in the Mouth".

[![](/../img/download.gif)](https://books2read.com/u/mvgA64)

* * * * *

## Trouble is My Client

[![](/../img/trouble-is-my-client.png "Trouble is My Client ebook")](https://books2read.com/u/b5OMj6)

Benny Cahill is back. After getting involved in a crooked horse race in Don't Look a Gift Horse in the Mouth, the cynical and wise-cracking private detective returns in four never before published short mysteries (Where There's a Will, Now Hear This, Reach for It, and To the Moon). These noir stories are inspired by the author's love of Old Time Radio shows. This collection includes a foreword by [Theresa Linden](https://www.theresalinden.com/), author of [*Chasing Liberty*](http://www.amazon.com/dp/B00OTNRZ16).

[![](/../img/buy.png)](https://books2read.com/u/b5OMj6)

* * * * *

## A Battle for the Faith

[![](/../img/a-battle-for-the-faith.jpg "A Battle for the Faith ebook")](https://books2read.com/u/38DgP6)

 Two short stories: A Matter of Silence and A Matter of Action. Emperor Kadzrick has decided that the Tribe of the Fathers much change their faith to suit him. The Tribe of the Fathers have held true to their Faith for centuries and will not change now. They are a peaceful tribe, but they will be forced to take up arms to defend what they hold dear: their faith. 
 
 This collection includes a story written by [Theresa Linden](https://www.theresalinden.com/), author of [*Chasing Liberty*](http://www.amazon.com/dp/B00OTNRZ16).

[![](/../img/buy.png)](https://books2read.com/u/38DgP6)

* * * * *

## Fire in the Night

![](/../img/fire-in-the-night-update-tumb.png "Fire in the Night ebook")

In the early 1860s, European headlines are filled with stories of attacks on the merchant vessels of many countries. Europe is filled with suspicion and paranoia. Is it pirates or a hostile power? Wealthy American playboy Tom Cavanaugh smells trouble and those who look for trouble, always get more than they bargained for. 
This is a preview of John Paul Wohlscheid's novel, *Rumors of War*, to be released Fall 2015.

[![](/../img/download.gif)](https://leanpub.com/fireinthenight)

* * * * *

## Letters Home

[![](/../img/letters-home.png "Letters Home ebook")](https://books2read.com/b/4NR509)

When a loved one marches off to war, those left behind suffer from worry and heartache. That is especially true when the loved one in question is an only child. Sending letters home is often a great comfort for those left behind. 
 
[![](/../img/buy.png)](https://books2read.com/b/4NR509)

* * * * *
  
# Nonfiction

## Church Triumphant: 25 Men and Women Who Gave Their Lives to Christ

[![](/../img/church-triumphant.jpg "Church Triumphant ebook")](https://books2read.com/u/mdp7Rm)

In Church Triumphant, you can read the lives of 25 men and women who dedicated their lives to God and the Catholic Church. These men and women are heroes of virtue and prayer who helped change the world for Christ. The biographies are written concisely with the goal of giving you the chance to learn about great saints in a short amount of time.

This book includes the following saints: St. Gonsalo Garcia, St. Andreas Wouters, Blessed Charles I of Austria, Blessed Charles de Foucauld, St. Clement Maria Hofbauer, St. Gabriel of Our Lady of Sorrows, St. Gerard Majella, St. Gianna Molla, Blessed John Henry Newman, St. Maria Goretti, St. Maximilian Kolbe, St. Nicholas, St Peter Julian Eymard, Blessed Pier Giorgio Frassati, St. Padre Pio of Pietrelcina, Pope St. Pius X, St. Teresa Benedicta of the Cross, St. Charbel Makhluf, Venerable Bishop Frederic Baraga, Venerable Fulton J. Sheen, Blessed José Luis Sánchez del Río, St. Kateri Tekakwitha, St. Marianne Cope, Blessed Miguel Pro, Servant of God Father Patrick Peyton

[![](/../img/buy.png)](https://books2read.com/u/mdp7Rm)
