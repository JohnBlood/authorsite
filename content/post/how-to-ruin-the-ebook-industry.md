---
title: "How to Ruin the Ebook Industry"
description: ""
date: 2012-12-19T21:12:31-04:00
publishDate: 2012-12-19T21:12:31-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [ebooks, publishing industry]
---

Recently, I was doing some research on ebook publishing when I came across an article by a man who obviously specializes in get-rich-quick schemes. The article was entitled *Make Money Selling Ebooks: How to Create Your Own Ebook Publishing Empire By Writing Just One Ebook Per Month*. In this article, the writer says the following:

> What if I told you that you could write a dozen ebooks, and turn them into 30 to 50 e-products, all making you money day in and day out? The beautiful thing about ebook publishing and writing is that once the publication is finished, it can earn you money for years to come. And, if you can literally double, triple or quadruple your output, without doubling, tripling or quadrupling your efforts -- well, now THAT'S a winning formula. As the author of somewhere around 18 ebooks to date, following is the formula I now "formally" follow...A pullout is simply a chapter or portion of your ebook that you can sell as an individual product. For example, my most recent ebook, How to Write an Ebook in 3 Days, Market It & Start Getting Sales within a Week -- Really! has four pullouts. These pullouts will sell anywhere from 99 cents to $9.95. The full ebook will sell for $19.95; the introductory price is $14.95 (for a very limited time)

To me, this is a very underhanded way of doing business. I would compare it to a dealership selling a car without the engines or wheels. There is only person it benefits is the author. It would make customer distrustful of ebook publishers, and it would also breed badly written content for a high price. To be blunt: this is a bad business practice and should be avoided at all costs.
