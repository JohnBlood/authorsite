---
title: "How to Make Your Main Character a Role Model"
description: ""
date: 2015-11-17T10:56:45-04:00
publishDate: 2015-11-17T10:56:45-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Tips, writing]
---

![](/img/lone-ranger-tonto.jpg "The Lone Ranger and Tonto")

One of the most important things that an author can do is create a main character that is a role model to his readers. To accomplish this, it is a good idea to create a list of what the character can and cannot do, so you can keep him on the straight and narrow. Let me give you an example of what I mean.

One of my favorite characters from Old Time Radio is the Lone Ranger. (The original radio program and TV show are the only ones worth seeing/listening to.) Fran Striker, co-creator and writer of the Lone Ranger, created the following creed for the Ranger:

> I believe…
>
> *   That to have a friend, a man must be one.
>
> *   That all men are created equal and that everyone has within himself the power to make this a better world.
>
> *   That God put the firewood there, but that every man must gather and light it himself.
>
> *   In being prepared physically, mentally, and morally to fight when necessary for what is right.
>
> *   That a man should make the most of what equipment he has.
>
> *   That ‘this government of the people, by the people, and for the people’ shall live always.
>
> *   That men should live by the rule of what is best for the greatest number.
>
> *   That sooner or later…somewhere…somehow…we must settle with the world and make payment for what we have taken.
>
> *   That all things change but truth, and that truth alone, lives on forever.
>
> *   In my Creator, my country, my fellow man

Striker and George W. Trendle, the other co-creator, also wrote these guidelines for the Lone Ranger.

> *   The Lone Ranger is never seen without his mask or a disguise.
>
> *   With emphasis on logic, The Lone Ranger is never captured or held for any length of time by lawmen, avoiding his being unmasked.
>
> *   The Lone Ranger always uses perfect grammar and precise speech completely devoid of slang and colloquial phrases, at all times.
>
> *   When he has to use guns, The Lone Ranger never shoots to kill, but rather only to disarm his opponent as painlessly as possible.
>
> *   Logically, too, The Lone Ranger never wins against hopeless odds; _i.e._, he is never seen escaping from a barrage of bullets merely by riding into the horizon.
>
> *   Even though The Lone Ranger offers his aid to individuals or small groups, the ultimate objective of his story never fails to imply that their benefit is only a by-product of a greater achievement—the development of the west or our country. His adversaries are usually groups whose power is such that large areas are at stake.
>
> *   Adversaries are never other than American to avoid criticism from minority groups. There were exceptions to this rule. He sometimes battled foreign agents, though their nation of origin was generally not named.
>
> *   Names of unsympathetic characters are carefully chosen, never consisting of two names if it can be avoided, to avoid even further vicarious association—more often than not, a single nickname is selected.
>
> *   The Lone Ranger never drinks or smokes and saloon scenes are usually interpreted as cafes, with waiters and food instead of bartenders and liquor.
>
> *   Criminals are never shown in enviable positions of wealth or power, and they never appear as successful or glamorous.

When the Lone Ranger held true to these guidelines, the Ranger was a strong role model (not like the remakes).

Not everyone writes westerns, but I think that this idea is still useful. A great role model can leave a lasting impact on the reader. There are too many negative characters and stereotypes in fiction. I think it's time we changed that.
