---
title: "Do eBook Covers Have to Cost?"
description: "Ebook covers don'e have to be expensive"
date: 2014-11-29T11:25:47-04:00
publishDate: 2014-11-29T11:25:47-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [tips]
---

If you ever published an ebook or just looked into publishing an ebook, one of the biggest costs is the cover. I've seen prices as low as $40 and as high as $300+. You could make your own ebook cover, especially if you're a graphic designer, but most people don't have that skill.

Luckily, there is this great place called Fiverr, where you can get all kinds of stuff starting at $10. I got my last ebook cover from a seller named [Angie](http://bit.ly/125rwln). Check out here gig. She does terrific work. Also, check out everything that Fiverr has to offer.
