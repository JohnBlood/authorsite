---
title: "Jack Webb in Pat Novak, for Hire"
description: ""
date: 2015-11-05T10:45:20-04:00
publishDate: 2015-11-05T10:45:20-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Jack Webb, otr]
---

![](/img/patnovakforhire.jpg "Jack Webb in Pat Novak, for Hire")

One of my biggest inspirations for my detectives stories is one of Jack Webb's early radio programs: Pat Novak, for Hire. Before his hit radio show (and later TV show) Dragnet, Webb was involved with several private detective radio programs that did not last very long. The first was Pat Novak, for Hire.

Webb plays Pat Novak, who is not a detective by trade. He mainly rents boats, but somehow he always ends up in the middle of a problem. Besides Webb, there were two other man characters. The first was Inspector Hellman (played by Raymond Burr) who threatens to haul Novak in whenever he finds him at the scene of the crime, whether he did it or not. The second was Jocko Madigan (played by Tudor Owen), a drunken ex-doctor who runs errands for Novak.

The show ran for 4 episodes in 1947 with Ben Morris playing the lead. Jack Webb took over for 19 episodes in 1949. Like I said, the show only ran for a short time, but it was fun. You can listen to [Pat Novak, for Hire here](https://archive.org/details/PatNovakForHire). What Old Time Radio shows do you enjoy? List them in the comments below.
