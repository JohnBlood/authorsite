---
title: "New eBook Released, New Project, and Other Coming Attractions"
description: ""
date: 2015-03-21T11:08:47-04:00
publishDate: 2015-03-21T11:08:47-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Announcement, eBooks, Benny Cahill]
---

**New Book Released**

I'm happy to announce the release of a new ebook, entitled _Church Triumphant: 25 Men and Women who Gave Their Lives to Christ_. This new ebook is available for [preorder from Amazon](http://www.amazon.com/Church-Triumphant-Women-Their-Christ-ebook/dp/B00UZDNN0Q/), [Barnes and Noble](http://www.barnesandnoble.com/w/church-triumphant-john-paul-wohlscheid/1121502684?ean=2940046645347), [Kobo](https://store.kobobooks.com/en-US/ebook/church-triumphant-25-men-and-women-who-gave-their-lives-to-christ), and [iTunes](https://itunes.apple.com/us/book/church-triumphant-25-men-women/id978786964?mt=11). _Church Triumphant_ will be officially released on April 5th. Easter Sunday.

![](/../img/church-triumphant.jpg "Church Triumphant: 25 Men and Women who Gave Their Lives to Christ")

**Current projects**

I'm currently working on a new Benny Cahill mystery for an Amazon bundle being put together by a number of fellow writers. I don't want to give away too much information, but it titled _All that Glitters is Not Gold_, and it involves Cahill in a museum. Keep your eyes out for information about the bundle. Until then, enjoy this new cover.

![](/../img/allthatglittersisnotgold-thumb.jpg "All that Glitters is Not Gold")

On top of everything else, I am working on a mystery novel collaboration project with two fellow writers. All I can say is that it is a noir story set in modern-day. We're still in the early stage of the project.

**Coming Attractions**

Up until now, this blog has mainly been a place for me to tell you about my new books and projects. Starting next week, I will start writing posts about my favorite Old Time Radio and TV programs. If you like my stories, you'll enjoy these great shows from the Golden Age of radio.
