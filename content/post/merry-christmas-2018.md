---
title: "Merry Christmas 2018"
description: ""
date: 2018-12-25T11:50:26-04:00
publishDate: 2018-12-25T11:50:26-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Christmas, Picture, Announcement]
---


Merry Christmas to All!
=======================

![](/img/santa-baby-jesus-250x300.jpg "Merry Christmas")

May God Bless you and your loved ones in the new year.
------------------------------------------------------

### John Paul Wohlcheid
