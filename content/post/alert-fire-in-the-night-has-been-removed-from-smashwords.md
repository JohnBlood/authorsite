---
title: "ALERT - Fire in the Night has been removed from Smashwords"
description: ""
date: 2015-02-03T00:59:34-04:00
publishDate: 2015-02-03T00:59:34-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Announcement, eBooks, Smashwords]
---

I just got a notification that Smashwords does not publish previews, they only publish complete stories. Since that was the case, I have unpublished _Fire in the Night_, thus it will not be available on iTunes, Barnes and Nobles, and more. Don't worry, though, I created an epub and PDF version of _Fire in the Night_. So come and get _Fire in the Night_ [PDF](https://dl.dropboxusercontent.com/u/2014278/Fire%20in%20the%20Night.final.pdf) or [epub](https://dl.dropboxusercontent.com/u/2014278/Fire%20in%20the%20Night%20-%20John%20Paul%20Wohlscheid.epub) here. Sorry for any inconvenience. The previous post has been corrected.
