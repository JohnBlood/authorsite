---
title: "New Cover for a New Ebook"
description: "The next Benny Cahill ebook now has a cover."
date: 2014-10-29T01:59:45-04:00
publishDate: 2014-10-29T01:59:45-04:00
author: "John Paul Wohlscheid"
images: [/img/trouble-is-my-client.png]
draft: false
type: post
tags: [Announcement, Benny-Cahill, ebook]
---

The next Benny Cahill ebook now has a cover. Angie at [Fiverr ](https://www.fiverr.com/pro_ebookcovers/design-an-eye-catching-ebook-or-kindle-cover-with-bonus?funnel=2014102901513715318985780)did a great job. If you need a book cover, check her out and tell her I sent you. The ebook is 85% done and on track for release in the early part of December.

![](/../img/trouble-is-my-client.png "Trouble is My Client ebook")

Stay tuned for more details as they become available.
