---
title: "Merry Christmas and a Couple Announcements"
description: ""
date: 2015-12-25T10:15:27-04:00
publishDate: 2015-12-25T10:15:27-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Christmas, sale, Announcement, ebooks]
---

![](/img/simple-merry-christmas.png "Merry Christmas")

I would like to wish everyone a Merry Christmas and a Happy and Holy New Year. May you and your loved ones be richly blessed in the coming New Year.

Now for the Announcements
-------------------------

The last couple of weeks have been very busy for me. Besides getting ready for Christmas, I co-published a new ebook with my friend Theresa Linden. The book (entitled [A Battle for the Faith](http://www.amazon.com/dp/B019QTQIH4)) contains two short stories, one written by each of us. Here is the synopsis:

![](/img/a-battle-for-the-faith.jpg "A Battle for the Faith")

>  Two short stories: A Matter of Silence and A Matter of Action. The Emperor Kadzrick has decided that the Tribe of the Fathers much change their faith to suit him. The Tribe of the Fathers have held true to their Faith for centuries and will not change now. They are a peaceful tribe, but they will be forced to take up arms to defend what they hold dear: their faith.

The book is currently available on Amazon, Smashwords, and elsewhere for 99 cents.

![](/img/JP-Christmas-Sale.jpg "JP Christmas Sale")

As a Christmas present, I have decided to have a little Christmas sale of my own and offer all my books (except the free ones) for 99 cents. This sale will end on January 2nd. So, if you got a new e-reader, you can get quality reading material for a low holiday price. You can visit my profile on [Amazon](http://www.amazon.com/John-Paul-Wohlscheid/e/B00U6DJRCI/) and [Smashwords](https://www.smashwords.com/profile/view/JohnBlood). As always, if you enjoy the books that you purchase, please review. Enjoy.
