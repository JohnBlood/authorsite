---
title: "Announcing the Newest Benny Cahill eBook on Smashwords"
description: ""
date: 2014-12-02T11:58:52-04:00
publishDate: 2014-12-02T11:58:52-04:00
author: "John Paul Wohlscheid"
images: [/img/trouble-is-my-client.png]
draft: false
type: post
tags: [Announcement, Benny-Cahill, Smashwords, ebooks]
---

After several months of hard work, I'm proud to announce that the newest stories of Benny Cahill, PI are now available on Smashwords. [*Trouble is My Client*](http://bit.ly/1wOvUTt) contains **FOUR** new and never before seen Benny Cahill stories. These stories (*Where There's a Will*, *Now Hear This*, *Reach for It*, and* To the Moon*) recall the days of Old Time Radio and the detective stories that formed a generation of Americans. From the Smashwords page:

> Benny Cahill is back. After getting involved in a crooked horse race in Don't Look a Gift Horse in the Mouth, the cynical and wise-cracking private detective returns in four never before published mysteries. These noir stories are inspired by the author's love of Old Time Radio shows. This collection includes a foreword by Theresa Linden, author of Chasing Liberty.

Click the book cover below to go to the book's Smashwords page. More ebook stores will be added soon. Don't forget to rate and review after you have read it.

[![](/../img/trouble-is-my-client.png "Trouble is My Client ebook")](https://www.smashwords.com/books/view/497797)

Click the cover for the Smashwords page.
