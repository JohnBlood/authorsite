---
title: "My Favorite Note Taking Apps"
description: ""
date: 2014-01-09T23:18:13-04:00
publishDate: 2014-01-09T23:18:13-04:00
author: "John Paul Wohlscheid"
images: [/img/google-keep.png]
draft: false
type: post
tags: [tools, writing]
---

Every writer needs a notebook to keep track of their story ideas and inspiration. While I like a good paper notebook, I love the ability to take notes with my mobile devices and have them available to use without having to type them out. Here are two of my favorites.

## Google Keep

![](/img/google-keep.png)

This app is great because it is light and very quick and easy to use. If you have a Google account, you are all ready to go. I put this app first because it automatically syncs with the web version when you connect to the web. You can color code your notes and use it to remember appointments. This app is available on Android and on the web @ [keep.google.com/](https://keep.google.com/u/0/). Give it a try.

## Evernote

![](/img/evernote1.png)

![](/img/evernote2.png)

If you don't have a Google account, don't like Google, or need apps that are available on iOS, as well as Android, then this is the app for you. Evernote has a few more features than Google Keep. You can create separate notebooks for different projects and add tags to find similar notes quicker. You can find Evernote here: [evernote.com.](http://www.evernote.com/)

Both of these apps are great, and I use both. Try them both and see which works best for you. What note-taking apps do you use?
