---
title: "Theresa Linden Releases Testing Liberty"
description: ""
date: 2015-11-08T15:57:19-04:00
publishDate: 2015-11-08T15:57:19-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [dystopian novels, Theresa Linden, Announcement]
---

![](/img/testing-liberty.jpg "Testing Liberty")

In January of this year, I published a two-part review of Theresa Linden's first novel, [Chasing Liberty](http://amzn.to/1HmRRhZ). (If you want to reread the review, you find it [here](http://realromancatholic.com/2015/01/13/chasing-liberty-a-review-part-1/) and [here](http://104.236.36.166/2015/01/19/chasing-liberty-a-review-part-2/).) Yesterday, Theresa released the second volume of the trilogy: Testing Liberty. Chasing Liberty ended with Liberty and others being captured by the government and sent to re-education facilities. From there, you are taken on a whirlwind ride of action and suspense.

Here is the summary from the Amazon page:

> Hidden no more. Imprisoned by the all-controlling government. Slated for Re-Education. Liberty must escape from a high-security facility to rescue the colonists who lost their freedom. Their capture is her fault. Set in the not-so-distant future, where the government controls society through indoctrination, population control, and the eradication of the family, Liberty bides her time in Aldonia’s Re-Education facility. If she fails to escape and rescue the others, the children, teens and adults of the Maxwell Colony will be integrated into society, facing sterilization and indoctrination. She is not alone in the desire to rescue the colonists. An underground, anti-government group has been rescuing people from Aldonia for years, but never have they attempted to rescue so many at one time. To do so would risk exposing, even ending, their operation. Dedrick, one of the top rescuers, grieves for his family members who are now residents of government’s facilities. He wants Liberty free, but he is opposed to working with her. Racing through the wild, the underground and sordid inner-city slums, Testing Liberty follows Liberty from one trial to another, to her final sacrifice.

From the destruction of the family to the planned reduction of the population in the name of protecting the environment, Theresa has produced a modern take on __A Brave New World__ based on the insanity she saw going on around her. This series is a warning of a future probable.

It is a great book written by a great author. And I'm not just saying that because I created a character for the book. I took the time to read this book and greatly enjoyed it. Several major characters went through major developments, proving that Theresa's characters are not flat and one dimensional. They could be real. Theresa has created a series of dystopian novels that are accessible to everyone. I can't wait to read the third and final book in the trilogy.

Theresa has written both books while being a busy homeschool mother. That takes dedication and it shows in her books.

Get out there and help support a talented, Catholic indie author. You can pick up your copy today (paperback or ebook) by clicking [here](http://amzn.to/1S9c3UZ).
