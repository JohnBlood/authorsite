---
title: "Goethe on Originality"
description: ""
date: 2014-09-11T03:09:19-04:00
publishDate: 2014-09-11T03:09:19-04:00
author: John Paul Wohlscheid"
images: [/img/goethequote.png]
draft: false
type: post
tags: [Goethe, picture, writer-quote]
---

![](/img/goethequote.png "Goethe on Originality")
