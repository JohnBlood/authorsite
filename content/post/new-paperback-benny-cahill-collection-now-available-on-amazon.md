---
title: "New Paperback Benny Cahill Collection Now Available on Amazon"
description: ""
date: 2020-03-02T23:35:43-05:00
publishDate: 2020-03-02T23:35:43-05:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Announcement, Benny Cahill, paperback]
---

![](/img/BCT-1-scaled.jpg "Benny Cahill, Troubleshooter paperback")

Benny Cahill is back in action, kind of. This post is to announce the release of a paperback collection of the Benny Cahill stories. The [_Benny Cahill, Troubleshooter_ paperback](https://www.amazon.com/dp/B0857BHDD6) contains the stories that you already enjoy (Don't Look a Gift Horse in the Mouth, Where There's a Will, Now Hear This, Reach for It, and To the Moon). It also includes an entirely new story written just for the paperback.

The new story is entitled "Brothers in Arms". This new short story is set after the events of "Reach for It". In this story, one of Cahill's old war buddies is accused of murder, and it's up to Cahill to prove the man's innocence.

As part of creating this paperback, I spent several months editing and rewriting the existing five Benny Cahill stories. I have updated them on all ebook marketplaces, so if you previously ordered a copy it should be updated soon.

If you have friends that enjoy noir detective stories, tell them about the [_Benny Cahill, Troubleshooter_ paperback](https://www.amazon.com/dp/B0857BHDD6). Remember that the Benny Cahill stories are designed to be flinch-free.

Benny Cahill added to a new store
---------------------------------

I have recently added the Benny Cahill stories (minus Brothers in Arms, which is a paperback exclusive) to a new ebook store. The store is named [Leanpub](https://leanpub.com/u/johnpaulw). Leanpub is primarily designed to help technically-minded people write and publish their own books. Because of this, many of the books that they offer are very technical. However, Leanpub has some tools that are very helpful to non-technical writers. I'm planning to cover the merits of Leanpub in e future article. Until then, take a couple of minutes to look at [my page on Leanpub](https://leanpub.com/u/johnpaulw).

Please share this post with your fellow detective story buffs.
