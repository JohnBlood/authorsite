---
title: "Ebook Now Available on Amazon"
description: "'Don't Look a Gift Horse in the Mouth' is now available on Amazon."
date: 2014-01-08T22:49:25-04:00
publishDate: 2014-01-08T22:49:25-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [ebooks, Amazon]
---

I would like to announce that my first ebook "Don't Look a Gift Horse in the Mouth" is now available on Amazon. You can find it here: [https://www.amazon.com/dp/B00HQ5INHE](https://www.amazon.com/dp/B00HQ5INHE). Please buy a copy and help a young writer with big dreams.
