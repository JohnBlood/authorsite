---
title: "Merry Christmas"
description: ""
date: 2014-12-25T19:29:00-04:00
publishDate: 2014-12-25T19:29:00-04:00
author: "John Paul Wohlscheid"
images: [/img/santababyjesus.gif]
draft: false
type: post
tags: [Christmas]
---

![](/../img/santababyjesus.gif "Merry Christmas")

I would like to wish everyone a Merry Christmas. I have many fun things planned for the coming year, so keep an eye on this blog.

Again, Merry Christmas,

John Paul W
