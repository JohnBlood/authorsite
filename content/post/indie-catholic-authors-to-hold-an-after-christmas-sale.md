---
title: "Indie Catholic Authors to Hold an After Christmas Sale"
description: ""
date: 2015-12-23T00:30:30-04:00
publishDate: 2015-12-23T00:30:30-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Christmas, sale, Announcement]
---

![](/img/christmas-2015-sale.jpg "2015 Indie Catholic Authors Christmas sale")

In June, the members of the Indie Catholic Authors group held an ebook sale. We're going to do that again this year after Christmas. From December 26-28, 16 Catholics will be available for 99 cents or free.

Here is the information from the official Indie Catholic Authors blog.

> Giving your loved one a Kindle, Nook, or iPad for Christmas? Help them fill it with great Catholic books! Join Indie Catholic Authors December 26-28 for our It’s Still Christmas Sale. 16 Catholic ebooks will be $.99 or free. That’s besides our permanently free ebooks!
>
> Participating authors
> ---------------------
>
> *   John C. Connell
> *   Jeanie Ewing
> *   Ellen Gable
> *   Melanie Jean Juneau
> *   Theresa Linden
> *   Gil Michelini
> *   Erin McCole Cupp
> *   Connie Rossini
> *   Marianne Sciucco
> *   Tim Speer
> *   Thomas Tan
> *   Jacqueline Vick
> *   J.I. Willett
> *   Gloria Winn
> *   John Paul Wohlschied
>
> On Saturday, December 26, here on our blog, we’ll have links to all the retailers where you can buy the books at sale prices. Plus, look for detailed descriptions of each title. You can get a whole library for about $10! Please note: individual authors are responsible for making sure their books are discounted. See you on the 26th!
