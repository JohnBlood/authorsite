---
title: "Next Project in the Works"
description: "Here is some information about the coming attractions"
date: 2014-12-08T12:04:00-04:00
publishDate: 2014-12-08T12:04:00-04:00
author: "John Paul Wohlscheid"
images: [/img/fire-in-the-night.png]
draft: false
type: post
tags: []
---

![](/../img/fire-in-the-night.png "Fire in the Night ebook")

I published my latest ebook about a week ago. I've already begun planning my next ebook. It will be at least a novella in length. It will be an Ocean's Eleven-style steampunk thriller. Here is the plot:

> In the late 1860s, ships in the English Channel are attacked at sunk by a mysterious ship in the night. Several navies have sent ships to investigate, but the found nothing. Most people are convinced the French are behind it. However,  a mysterious American adventurer, named Tom Cavanaugh, isn't so sure and he's putting together a team to investigate.

First, I'm going to publish a short prequel entitled *Fire in the Night*, so readers can get a taste of things to come. The full novel will follow. I'm not setting a deadline until the book is 90%+ done. I've come up with the chapter titles, so I'm good to go. I hope.

Stay tuned for further announcements.
