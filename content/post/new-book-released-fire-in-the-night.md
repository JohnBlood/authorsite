---
title: "New Book Released - Fire in the Night!!!"
description: ""
date: 2015-02-01T14:33:00-04:00
publishDate: 2015-02-01T14:33:00-04:00
author: "John Paul Wohlscheid"
images: [/img/fire-in-the-night-update-tumb.png]
draft: false
type: post
tags: [Announcement, eBooks]
---

![](/../img/fire-in-the-night-update-tumb.png "Fire in the Night ebook")

I am proud to announce the release of another ebook. This ebook, entitled _Fire in the Night_, is a preview of an upcoming novel to be released Fall 2015. My upcoming novel, _Rumors of War_, will be my first finished novel, so I hope you look forward to it. Here is the summary from _Fire in the Night_:

> In the early 1860s, European headlines are filled with stories of attacks on the merchant vessels of many countries. Europe is filled with suspicion and paranoia. Is it pirates or a hostile power? Wealthy American playboy Tom Cavanaugh smells trouble and those who look for trouble, always get more than they bargain for.

Currently, _Fire in the Night_ is only available as [PDF](https://dl.dropboxusercontent.com/u/2014278/Fire%20in%20the%20Night.final.pdf) or [epub](https://dl.dropboxusercontent.com/u/2014278/Fire%20in%20the%20Night%20-%20John%20Paul%20Wohlscheid.epub).  Thanks.
