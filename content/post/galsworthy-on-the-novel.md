---
title: "Galsworthy on the Novel"
description: ""
date: 2014-12-09T11:34:00-04:00
publishDate: 2014-12-09T11:34:00-04:00
author: "John Paul Wohlscheid"
images: [/img/johngalsworthyquote.png]
draft: false
type: post
tags: [John-Galsworthy, picture, writer-quote]
---

![](/../img/johngalsworthyquote.png "Galsworthy on the Novel")
