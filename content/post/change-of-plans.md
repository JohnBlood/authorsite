---
title: "Change of Plans"
description: ""
date: 2014-07-27T12:30:19-04:00
publishDate: 2014-07-27T12:30:19-04:00
author: "John Paul Wohlscheid"
images: []
draft: false
type: post
tags: [Announcement]
---

In Thursday's post, I announced the release of a new Benny Cahill story on September 15. After some thought, I have decided to instead release of a collection of three short stories to give readers a better deal. The Benny Cahill trilogy will be released on December 1, with pre-order starting two weeks earlier. I will keep you updated

I would also like to announce the release of another series of mysteries, the Father Benedict Granger series. These stories are based around Fr. Granger, who was a private detective earlier in life before becoming a man of the cloth. The first story will be released for free on September 5. A collection of stories will soon follow.

Please stay tuned to this blog for further updates.
